﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Network
{
    class TcpSocket : Socket
    {
        private TcpClient _client;

        public TcpSocket(TcpClient client)
        {
            _client = client;
        }
        public override void Write(byte[] data)
        {
            NetworkStream leStream = _client.GetStream();
            leStream.Write(data, 0, data.Length);
        }

        public override int ReadByte()
        {
            NetworkStream leStream = _client.GetStream();
            return leStream.ReadByte();
        }

        public override void Close()
        {
            _client.Close();
        }

        public override Stream GetStream()
        {
            return _client.GetStream();
        }
    }
}
