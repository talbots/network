﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network
{
    class WritableSocket:
        IWrite
    {
        private Socket _soc;
        public WritableSocket(Socket soc)
        {
            _soc = soc;
        }

        public void Write(byte[] data)
        {
            _soc.Write(data);
        }
    }
}
