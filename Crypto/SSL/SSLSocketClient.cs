﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network.Crypto.SSL
{
    class SSLSocketClient : SSLSocket
    {
        public SSLSocketClient(Socket soc, SSLConfigClient config) : base(soc, config)
        {
            _stream.AuthenticateAsClient(
                config.ServerName
                , config.ClientCertificates
                , config.SSLProtocol
                , config.CheckCertificateRevocation
                );

        }
    }
}
