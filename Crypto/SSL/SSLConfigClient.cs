﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Network.Crypto.SSL
{
    class SSLConfigClient: SSLConfig
    {
        public string ServerName { get; set; }

        public X509CertificateCollection ClientCertificates { get; set; } = null;


        public SSLConfigClient(string serverName)
        {
            ServerName = serverName;
        }
    }
}
