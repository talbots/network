﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Net.Sockets;

namespace Network.Crypto.SSL
{
    abstract class SSLSocket : Socket
    {
        protected Socket _soc;
        protected SslStream _stream;
        public SSLSocket(Socket soc, SSLConfig config)
        {
            _soc = soc;
            _stream = new SslStream(
                _soc.GetStream()
                ,config.LeaveInnerStreamOpen
                , config.RemoteCertificateValidationCallback
                , config.LocalCertificateSelectionCallback
                );
            _stream.ReadTimeout = config.ReadTimeout;
            _stream.WriteTimeout = config.WriteTimeout;
        }
        public override void Close()
        {
            _soc.Close();
        }
        public override int ReadByte()
        {
            return _stream.ReadByte();
        }

        public override void Write(byte[] data)
        {
            _stream.Write(data);
        }
        public override Stream GetStream()
        {
            return _soc.GetStream();
        }
    }
}
