﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Security;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;

namespace Network.Crypto.SSL
{
    class SSLConfig
    {
        public bool LeaveInnerStreamOpen { get; set; } = false;
        public SslProtocols SSLProtocol { get; set; } = SslProtocols.Tls;

        public int ReadTimeout { get; set; } = 5000;
        public int WriteTimeout { get; set; } = 5000;

        public bool CheckCertificateRevocation { get; set; } = true;

        public RemoteCertificateValidationCallback RemoteCertificateValidationCallback { get; set; } = null;

        public LocalCertificateSelectionCallback LocalCertificateSelectionCallback { get; set; } = null;

        public SSLConfig()
        {
        }
    }
}
