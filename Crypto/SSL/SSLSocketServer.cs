﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network.Crypto.SSL
{
    class SSLSocketServer : SSLSocket
    {
        public SSLSocketServer(Socket soc, SSLConfigServer config) : base(soc, config)
        {
            _stream.AuthenticateAsServer(
                config.Certificate
                , config.ClientCertificateRequired
                , config.SSLProtocol
                , config.CheckCertificateRevocation
                );
        }
    }
}
