﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Network.Messaging
{
    class Procedure
    {
        private Func<Socket, Object, Network.Next> _start;
        private List<Func<Socket, Object, Network.Next>> _steps;
        private Func<Socket, Object, Network.Next> _end;

        public Network.Procedure LaProcedure
        {
            get
            {
                Network.Procedure laProcedure = new Network.Procedure();
                if(_start != null)
                {
                    laProcedure.AddStep(_start);
                }
                for (int i = 0; i < _steps.Count; i++)
                {
                    laProcedure.AddStep(_steps[i]);
                }
                if(_end != null)
                {
                    laProcedure.AddStep(_end);
                }
                return laProcedure;
            }
        }
        public Func<Socket, Object, Network.Next> Middleware
        {
            get
            {
                return new Func<Socket, Object, Network.Next>((soc, obj) =>
                {
                    Network.Next leNext = new Network.Next(soc, obj);
                    if(_start != null)
                    {
                        leNext = _start(leNext.Socket, leNext.Data);
                    }
                    for (int i = 0; i < _steps.Count; i++)
                    {
                        leNext = _steps[i](leNext.Socket, leNext.Data);
                    }
                    if(_end != null)
                    {
                        leNext = _end(leNext.Socket, leNext.Data);
                    }
                    return leNext;
                });
            }
        }
        public Procedure()
        {
            _start = null;
            _steps = new List<Func<Socket, Object, Network.Next>>();
            _end = null;
        }
        public void AddStart(Func<object, Next> fn)
        {
            _start = (Socket soc, object data) =>
            {
                Next leNext;
                leNext = fn(null);
                Encoding.Write(new WritableSocket(soc), leNext.Message);
                return new Network.Next(soc, leNext.Data);
            };
        }
        public void AddEnd(Func<byte[], object, object> fn)
        {
            _end = (Socket soc, object data) =>
            {
                byte[] leMessage = new byte[0];
                bool valide = true;
                try
                {
                    leMessage = Messaging.Encoding.Read(new ReadableSocket(soc));
                }
                catch
                {
                    valide = false;
                }
                object leNext;
                if (valide)
                {
                    leNext = fn(leMessage, data);
                    return new Network.Next(soc, leNext);
                }
                throw new Exception("Le Message transmis est invalide");
            };
        }
        public void AddStep(Func<byte[], object, Next> fn)
        {

            _steps.Add((Socket soc, object data) =>
            {
                byte[] leMessage = new byte[0];
                bool valide = true;
                try
                {
                    leMessage = Messaging.Encoding.Read(new ReadableSocket(soc));
                }
                catch
                {
                    valide = false;
                }
                Next leNext;
                if (valide)
                {
                    leNext = fn(leMessage, data);
                    Encoding.Write(new WritableSocket(soc), leNext.Message);
                    return new Network.Next(soc, leNext.Data);
                }
                throw new Exception("Le Message transmis est invalide");
            });
        }
    }
}
