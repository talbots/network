﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network.Messaging
{
    class Next
    {
        public byte[] Message { get; }
        public object Data { get; }
        public Next(byte[] message, object data)
        {
            Message = message;
            Data = data;
        }
    }
}
