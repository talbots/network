﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Network.Messaging
{
    static class Encoding
    {
        public static void Write(WritableSocket stream, byte[] lesBytes)
        {
            int length = lesBytes.Length;
            string characters = length.ToString();
            byte[] lesChars = System.Text.Encoding.ASCII.GetBytes(characters);
            byte[] message = new byte[lesChars.Length + 1 + lesBytes.Length];
            int i;
            for (i = 0; i < lesChars.Length; i++)
            {
                message[i] = lesChars[i];
            }
            message[i] = System.Text.Encoding.ASCII.GetBytes("\n")[0];
            i++;
            for (int j = 0; j < lesBytes.Length; j++)
            {
                message[i] = lesBytes[j];
                i++;
            }
            stream.Write(message);
        }
        public static byte[] Read(ReadableSocket stream)
        {
            List<byte> nbBytes = new List<byte>();
            int smallData;
            int endline = System.Text.Encoding.ASCII.GetBytes("\n")[0];
            while ((smallData = stream.ReadByte()) != endline)
            {
                if (smallData != -1)
                {
                    nbBytes.Add((byte)smallData);
                }
            }
            byte[] dataLength = nbBytes.ToArray();
            string asciiChars = System.Text.Encoding.ASCII.GetChars(dataLength).ToString();
            int messageLength;
            bool valide = int.TryParse(asciiChars, out messageLength);
            byte[] leMessage;
            int i = 0;
            int leByte;
            if (valide && messageLength >= 0)
            {
                leMessage = new byte[messageLength];
                while (i < messageLength)
                {
                    leByte = stream.ReadByte();
                    if (leByte != -1)
                    {
                        leMessage[i] = (byte)leByte;
                        i++;
                    }
                }

            }
            else
            {
                throw new Exception("Longueur du message non valide");
            }
            return leMessage;
        }
        public static byte[] UTF8ToASCII(string str)
        {
            byte[] byteHeader = new byte[str.Length];
            for (int k = 0; k < str.Length; k++)
            {
                if (str[k] < 256)
                {
                    byteHeader[k] = (byte)(int)str[k];
                }
                else
                {
                    throw new Exception("Chaine ASCII Invalide");
                }
            }
            return byteHeader;
        }
    }
}
