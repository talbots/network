﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network
{
    class ReadableSocket :
        IRead
    {
        private Socket _soc;
        public ReadableSocket(Socket soc)
        {
            _soc = soc;
        }
        public int ReadByte()
        {
            return _soc.ReadByte();
        }
    }
}
