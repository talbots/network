﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;

namespace Network
{
    abstract class Socket:
        IRead, IWrite
    {
        public abstract void Write(byte[] data);
        public abstract int ReadByte();
        public abstract void Close();
        public abstract Stream GetStream();
    }
}
