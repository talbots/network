﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Network
{
    abstract class Server
    {
        private Procedure _laProcedure;
        private int _port;
        public Server(int port, Procedure laProcedure)
        {
            _port = port;
            _laProcedure = laProcedure;
        }
        public void Start()
        {
            Task.Run(() => Run());
        }
        private void Run()
        {
            TcpListener server = new TcpListener(IPAddress.Parse("127.0.0.1"), this._port);
            server.Start();
            while (true)
            {
                TcpClient client = server.AcceptTcpClient();
                TcpSocket soc = new TcpSocket(client);
                Task.Run(() => Proccess(soc));
            }
        }
        private void Proccess(Socket soc)
        {
            Next leNext = new Network.Next(soc, null);
            bool valide = true;
            int i = 0;
            while(valide && i < _laProcedure.Steps)
            {
                try
                {
                    leNext = _laProcedure.GetStep(i)(leNext.Socket, leNext.Data);
                }
                catch
                {
                    valide = false;
                }
                i++;
                
            }
            if (valide)
            {
                IncomingSocket(leNext.Socket, leNext.Data);
            }
            soc.Close(); 
        }
        protected abstract void IncomingSocket(Socket soc, Object data);
    }
}
