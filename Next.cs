﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Network
{
    class Next
    {
        public Socket Socket { get; }
        public Object Data { get; }
        public Next(Socket soc, Object data)
        {
            Socket = soc;
        }
    }
}
