﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network.Request.Methods
{
    class Method:
        IConvertToByte
    {
        private Queue<string> _theQueue;
        public Method(string str)
        {
            _theQueue = new Queue<string>();
            string currentMethod = "";
            for(int i = 0; i < str.Length; i++)
            {
                if(str[i] == '.')
                {
                    Add(currentMethod);
                    currentMethod = "";
                }
                else
                {
                    currentMethod += str[i];
                }
            }
            Add(currentMethod);
        }
        public Method()
        {
            _theQueue = new Queue<string>();
        }
        public void Add(string path)
        {
            for (int i = 0; i < path.Length; i++)
            {
                if(path[i] == '\n')
                {
                    throw new Exception("Ajout impossible la méthode contient un retour ligne");
                }
                if(path[i] >= 256)
                {
                    throw new Exception("Ajout impossible caractère non ASCII");
                }
            }
            _theQueue.Enqueue(path);
        }

        public string Next()
        {
            return _theQueue.Dequeue();
        }

        public byte[] ToByte()
        {
            string method = "";
            string currentMethod;
            for(int i = 0; i < _theQueue.Count; i++)
            {
                currentMethod = _theQueue.Dequeue();
                _theQueue.Enqueue(currentMethod);
                method += currentMethod;
                if(i < (_theQueue.Count - 1))
                {
                    method += '.';
                }
            }
            byte[] lesBytes = new byte[method.Length];
            for(int i = 0; i < method.Length; i++)
            {
                if(((int)method[i]) < 256)
                {
                    lesBytes[i] = (byte)(int)method[i];
                }
                else
                {
                    throw new Exception("Caractère invalide");
                }
            }
            return lesBytes;
        }

        public int Left
        {
            get
            {
                return _theQueue.Count;
            }
        }
    }
}
