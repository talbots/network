﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Network.Request.Methods
{
    class Client : Network.Request.Client
    {
        public Client(IPAddress ip, int port, Procedure process) : base(ip, port, process)
        {
        }
        public byte[] Call(Method laMethode, byte[] body)
        {
            byte[] methodInByte = laMethode.ToByte();
            byte[] backData = new byte[methodInByte.Length + 1 + body.Length];
            int j = 0;
            for (int i = 0; i < methodInByte.Length; i++)
            {
                backData[j] = methodInByte[i];
                j++;
            }
            backData[j] = System.Text.Encoding.ASCII.GetBytes("\n")[0];
            j++;
            for (int i = 0; i < body.Length; i++)
            {
                backData[j] = body[i];
                j++;
            }
            byte[] message = Call(backData);
            List<Action> failCollection = new List<Action>();
            failCollection.Add(() =>
            {
                throw new Exception("En tête invalide");
            });
            failCollection.Add(() =>
            {
                throw new Exception("Méthode invalide");
            });
            DataProcessor leProcesseur = new DataProcessor(failCollection);
            return leProcesseur.Process(message);
        }
    }
}
