﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network.Request.Methods.JSON
{
    class JSON<T, U>
    {
        private Func<T, U> _fn;
        public JSON(Func<T, U> fn)
        {
            _fn = fn;
        }
        public byte[] Respond(byte[] data)
        {
            string strData = System.Text.Encoding.UTF8.GetChars(data).ToString();
            T obj;
            U backObj;
            string backData;
            try
            {
                obj = JsonConvert.DeserializeObject<T>(strData);
                try
                {
                    backObj = _fn(obj);
                    backData = JsonConvert.SerializeObject(new JSONMessage<U>(backObj), Formatting.Indented);

                }
                catch(JSONException e)
                {
                    JSONError backError = new JSONError(e);
                    backData = JsonConvert.SerializeObject(backError, Formatting.Indented);
                }
                return new OKHeader(Encoding.UTF8.GetBytes(backData)).ToByte();
            }
            catch
            {
                return new FailHeader().ToByte();
            }
        }

    }
}
