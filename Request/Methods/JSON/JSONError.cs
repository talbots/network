﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network.Request.Methods.JSON
{
    class JSONError
    {
        public JSONException Error { get; }
        public JSONError(JSONException error)
        {
            Error = error;
        }
        
    }
}
