﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network.Request.Methods.JSON
{
    class JSONException : Exception
    {
        public string Title { get; }
        public override string Message { get; }
        public JSONException(string title, string message)
        {
            Title = title;
            Message = message;
        }
    }
}
