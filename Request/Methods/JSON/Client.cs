﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Network.Request.Methods;

namespace Network.Request.Methods.JSON
{
    class Client :
        Network.Request.Methods.Client
    {
        public Client(IPAddress ip, int port, Procedure process) : base(ip, port, process)
        {
        }

        public TReceive Call<TSend, TReceive>(Method laMethode, TSend obj)
        {
            byte[] data = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(obj));
            byte[] backData = Call(laMethode, data);
            List<Action> laListe = new List<Action>();
            laListe.Add(() =>
            {
                throw new Exception("Problème au traitement des données");
            });
            DataProcessor leProcesseur = new DataProcessor(laListe);
            string finalData = Encoding.UTF8.GetString(leProcesseur.Process(backData));
            JSONMessage<TReceive> leMessage;
            JSONError lErreur;
            try
            {
                leMessage = JsonConvert.DeserializeObject<JSONMessage<TReceive>>(finalData);
                return leMessage.Message;
            }
            catch
            {
                try
                {
                    lErreur = JsonConvert.DeserializeObject<JSONError>(finalData);
                    throw lErreur.Error;
                }
                catch
                {
                }               
            }
            throw new Exception("Les données renvoyés ne sont pas valides.");     
        }
    }
}
