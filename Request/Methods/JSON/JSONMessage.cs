﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network.Request.Methods.JSON
{
    class JSONMessage<T>
    {
        public T Message { get; }
        public JSONMessage(T message)
        {
            Message = message;
        }
    }
}
