﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network.Request.Methods
{
    class MessageWithHeader
    {
        private string _header;
        private byte[] _message;
        public string Header
        {
            get
            {
                return _header;
            }
        }
        public byte[] Message
        {
            get
            {
                return _message;
            }
        }
        public MessageWithHeader(byte[] data)
        {
            bool found = false;
            int i = 0;
            string header = "";
            byte[] message;
            int k;
            while(!found && i < data.Length)
            {
                if(data[i] == System.Text.Encoding.ASCII.GetBytes("\n")[0])
                {
                    found = true;
                }
                else
                {
                    header += System.Text.Encoding.ASCII.GetChars(new byte[1] { data[i] });
                }
                i++;
            }

            if (found)
            {
                message = new byte[data.Length - i];
                k = 0;
                for(int j = i; j < data.Length; j++)
                {
                    message[k] = data[j];
                    k++;
                }
                _header = header;
                _message = message;
            }
            else
            {
                throw new Exception("Aucun séparateur entre l'entête et le message");
            }
        }
        public MessageWithHeader(string header, byte[] message)
        {
            _header = header;
            _message = message;
        }
        public byte[] ToByte()
        {
            byte[] byteHeader = Network.Messaging.Encoding.UTF8ToASCII(_header);
            byte[] backData = new byte[byteHeader.Length + 1 + _message.Length];
            int i;
            for(i = 0; i < byteHeader.Length; i++)
            {
                backData[i] = byteHeader[i];
            }
            backData[i] = (byte)'\n';
            i++;
            for(int j = 0; j < _message.Length; j++)
            {
                backData[i] = _message[j];
                i++;
            }
            return backData;  
        }

    }
}
