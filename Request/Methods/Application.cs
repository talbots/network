﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network.Request.Methods
{
    class Application
    {
        private Router _router;
        public Application(Router router)
        {
            _router = router;
        }
        public Func<byte[], byte[]> Process
        {
            get
            {
                return new Func<byte[], byte[]>((byte[] inData) =>
                        {
                            MessageWithHeader incomingRequest;
                            try
                            {
                                incomingRequest = new MessageWithHeader(inData);
                                Method laMethode = new Method(incomingRequest.Header);
                                return new OKHeader(_router.Respond(laMethode, incomingRequest.Message)).ToByte();
                            }
                            catch
                            {
                                return new FailHeader().ToByte();
                            }
                        }
                    );
            }
        }
    }
}
