﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network.Request.Methods
{
    class DataProcessor
    {
        public List<Action> FailCollection { get; set; }
        public DataProcessor(List<Action> failCollection)
        {
            FailCollection = failCollection;
        }
        public byte[] Process(byte[] data)
        {
            byte[] content = data;
            MessageWithHeader leMessage = null;
            bool valide = true;
            int i = 0;
            while(valide && i < FailCollection.Count)
            {
                leMessage = new MessageWithHeader(content);

                if(leMessage.Header != "OK")
                {
                    valide = false;
                    if(leMessage.Header == "Fail")
                    {
                        FailCollection[i]();
                    }
                    else
                    {
                        throw new Exception("En tête invalide à l'étape" + i);
                    }
                }
                content = leMessage.Message;
                i++;
            }
            if (valide)
            {
                throw new Exception("Exception non lancé par l'action.");
            }
            return content;
        }
    }
}
