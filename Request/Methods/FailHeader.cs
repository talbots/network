﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network.Request.Methods
{
    class FailHeader : MessageWithHeader
    {
        public FailHeader(): base("Fail", new byte[0])
        {

        }
    }
}
