﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network.Request.Methods
{
    class PathObject
    {
        public string Path { get; set; }
        public object Object { get; set; }
        public PathObject(string path, object obj)
        {
            Path = path;
            Object = obj;
        }

    }
}
