﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network.Request.Methods
{
    class Router
    {
        private Dictionary<string, Router> _routers;
        private Dictionary<string, Route> _routes;
        public Router()
        {
            _routers = new Dictionary<string, Router>();
            _routes = new Dictionary<string, Route>();
        }
        public void AddRouter(string path, Router router)
        {
            _routers.Add(path, router);
        }
        public void AddRoute(string path, Route route)
        {
            _routes.Add(path, route);
        }
        public byte[] Respond(Method method, byte[] message)
        {
            string path = method.Next();
            Router router;
            Route route;
            if(method.Left == 0)
            {
                if (_routes.ContainsKey(path))
                {
                    route = _routes[path];
                    return new OKHeader(route.Respond(message)).ToByte();
                }
                else
                {
                    return new FailHeader().ToByte();
                }
            }
            else
            {
                if (_routers.ContainsKey(path))
                {
                    router = _routers[path];
                    return router.Respond(method, message);
                }
                else
                {
                    return new FailHeader().ToByte();
                }
            }
        }   
    }
}
