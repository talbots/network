﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network.Request.Methods
{
    abstract class Route
    {
        public abstract byte[] Respond(byte[] message);
        
    }
}
