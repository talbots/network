﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Network.Request
{
    class Server : Network.Server
    {
        private Func<byte[], byte[]> _fn;
        public Server(int port, Func<byte[], byte[]> fn): base(port, new Network.Procedure())
        {
            _fn = fn;
        }
        public Server(int port, Func<byte[], byte[]> fn, Network.Procedure procedure) : base(port, procedure)
        {
            _fn = fn;
        }
        protected override void IncomingSocket(Socket soc, object data)
        {
            byte[] leMessage = new byte[0];
            byte[] backBytes;
            bool valide = true;
            while (valide)
            {
                try
                {
                    leMessage = Messaging.Encoding.Read(new ReadableSocket(soc));
                }
                catch
                {
                    valide = false;
                }
                if (valide)
                {
                    backBytes = this._fn(leMessage);
                    Messaging.Encoding.Write(new WritableSocket(soc), backBytes);
                }
            }
            soc.Close();
        }
    }
}
