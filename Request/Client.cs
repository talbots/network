﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Network.Request
{
    class Client : Network.Client
    {
        public Client(IPAddress ip, int port, Procedure process): base(ip, port, process)
        {
        }
        public byte[] Call(byte[] data)
        {
            Messaging.Encoding.Write(new WritableSocket(Socket), data);
            return Messaging.Encoding.Read(new ReadableSocket(Socket));
        }
    }
}
