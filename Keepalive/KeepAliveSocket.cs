﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Network.Keepalive
{
    class KeepAliveSocket : Socket
    {
        private Socket _soc;
        private Timer _timer;
        private Timer _intervalTimer;
        private Queue<byte> _byteQueue;
        private Queue<Action> _ToDo;
        private bool Dead { get; set; }
        int _byteCount = -1;
        int _iByte = 0;


        public KeepAliveSocket(Socket soc, Action action, double timeout, double interval)
        {
            _soc = soc;
            _ToDo = new Queue<Action>();
            _byteQueue = new Queue<byte>();
            _timer = new Timer(timeout);
            Dead = false;
            _timer.Elapsed += (sender, e) =>
            {
                Dead = true;
                _timer.Close();
                _intervalTimer.Close();
                _soc.Close();
                action();
            };
            _timer.Start();
            Task.Run(() =>
            {
                int tempoByte;
                while (!Dead)
                {
                    tempoByte = _soc.ReadByte();
                    if(tempoByte != -1)
                    {
                        _timer.Stop();
                        _timer.Start();
                        _byteQueue.Enqueue((byte)tempoByte);
                    }

                }
            });
            Task.Run(() =>
            {
                Action task;
                while (!Dead)
                {
                    if(_ToDo.Count > 0)
                    {
                        task = _ToDo.Dequeue();
                        task();
                    }
                }
            });
            _intervalTimer = new Timer(interval);
            _intervalTimer.Elapsed += (sender, e) =>
            {
                SendKeepAlive();
            };

        }

        public override void Close()
        {
            _soc.Close();
        }

        public override int ReadByte()
        {
            byte theByte;
            if(_byteQueue.Count > 0)
            {
                theByte = _byteQueue.Dequeue();
                if(_byteCount == -1)
                {
                    if(theByte != 0)
                    {
                        _byteCount = theByte;
                        _iByte = 0;
                    }
                }
                else
                {
                    if(_iByte < _byteCount)
                    {
                        _iByte++;
                        return theByte;
                    }
                    else
                    {
                        _byteCount = -1;
                        _iByte = 0;
                    }
                }
            }
            return -1;
        }

        public void WriteData(byte[] data)
        {
            int length = data.Length;
            int i = 0;
            while(length > 0)
            {
                if(length >= 255)
                {
                    _soc.Write(new byte[1] { 255 });
                    for(int j = 0; j < 255; j++)
                    {
                        _soc.Write(new byte[1] { data[i] });
                        i++;
                    }
                    length -= 255;
                }
                else
                {
                    _soc.Write(new byte[1] { (byte)data.Length });
                    for(int j = 0; j < length; j++)
                    {
                        _soc.Write(new byte[1] { data[i] });
                        i++;
                    }
                    length = 0;
                }
            }

        }
        public void SendKeepAlive()
        {
            _ToDo.Enqueue(() =>
            {
                _soc.Write(new byte[1] { 0 });
            });
        }
        public override void Write(byte[] data)
        {
            _ToDo.Enqueue(() =>
            {
                WriteData(data);
            });
        }

        public override Stream GetStream()
        {
            return _soc.GetStream();
        }
    }
}
