﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network.Keepalive
{
    enum Binary
    {
        Zero,
        One
    }
    static class Encoding
    {
        public static Binary[] ToBinary(int num)
        {
            List<Binary> lesBinaires = new List<Binary>();
            Binary tempo;
            while (num != 0)
            {
                tempo = (Binary)(num % 2);
                lesBinaires.Add(tempo);
                if(tempo == (Binary)1)
                {
                    num -= 1;
                }
                num = num / 2;
            }
            Binary[] tabBinaries = new Binary[lesBinaires.Count];
            for(int i = 0; i < lesBinaires.Count; i++)
            {
                tabBinaries[i] = lesBinaires[lesBinaires.Count - 1 - i];
            }
            return tabBinaries;
        }
        public static Binary[] ToBinary(byte aByte)
        {
            Binary[] backInt = ToBinary((int)aByte);
            Binary[] byteInBinary = new Binary[8];
            int j = 8 - backInt.Length; 
            for(int i = 0; i < backInt.Length; i++)
            {
                byteInBinary[j] = backInt[i];
                j++;
            }
            return byteInBinary;
        }
        public static int ToInt(Binary[] lesBinaires)
        {
            int num = 0;
            for(int i = 0; i < lesBinaires.Length; i++)
            {
                num += ((int)lesBinaires[lesBinaires.Length - 1 - i]) * (int)Math.Pow(2, i);
            }
            return num;
        }
        public static byte ToByte(Binary[] lesBinaires)
        {
            int num = ToInt(lesBinaires);
            if(num < 256)
            {
                return (byte)num;
            }
            throw new Exception("Binary array too long");
        }
        public static byte[] CutInBytes(Queue<Binary> laQueue, int n)
        {
            byte[] byteArray = new byte[n];
            Binary[] byteInBinary = null;
            int j = 0;
            for(int i = 0; i < (n*8); i++)
            {
                if((i % 8) == 0)
                {
                    if(byteInBinary != null)
                    {
                        byteArray[i / 8 - 1] = ToByte(byteInBinary);
                    }
                    byteInBinary = new Binary[8];
                    j = 0;
                }
                byteInBinary[j] = laQueue.Dequeue();
                j++;
            }
            return byteArray;
        }
    }
}
