﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Network.Keepalive
{
    class Procedure
    {
        private Action _action;
        private double _timeout;
        private double _interval; 
        public Procedure(Action action, double timeout, double interval)
        {
            _action = action;
            _timeout = timeout;
            _interval = interval;
        }
        public Func<Socket, Object, Network.Next> Middleware
        {
            get
            {
                return new Func<Socket, object, Next>((soc, obj) =>
                {
                    return new Network.Next(new KeepAliveSocket(soc, _action, _timeout, _interval), obj);
                });
            }
        }
    }
}
