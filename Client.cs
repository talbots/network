﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Network
{
    abstract class Client
    {
        private Procedure _laProcedure;
        private Socket _soc;
        public Socket Socket
        {
            get
            {
                return _soc;
            }
        }
        private Object _obj;
        public Object Object
        {
            get
            {
                return _obj;
            }
        }
        public Client(IPAddress ip, int port, Procedure process)
        {
            TcpClient leClient = new TcpClient();
            leClient.Connect(ip, port);
            TcpSocket soc = new TcpSocket(leClient);
            _laProcedure = process;
            Process(soc);
        }
        private void Process(Socket soc)
        {
            Next leNext = new Network.Next(soc, null);
            int i = 0;
            while (i < _laProcedure.Steps)
            {
               leNext = _laProcedure.GetStep(i)(leNext.Socket, leNext.Data);
               i++;
            }
            _soc = leNext.Socket;
            _obj = leNext.Data;
        }
    }
}
