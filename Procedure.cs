﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Network
{
    class Procedure
    {
        public int Steps
        {
            get
            {
                return _lesMethodes.Count;
            }
        }
        private List<Func<Socket, Object, Next>> _lesMethodes;
        public Procedure()
        {
            _lesMethodes = new List<Func<Socket, object, Next>>();
        }
        public void AddStep(Func<Socket, Object, Next> fn)
        {
            _lesMethodes.Add(fn);
        }
        public Func<Socket, Object, Next> GetStep(int i)
        {
            return _lesMethodes[i];
        }

    }
}
